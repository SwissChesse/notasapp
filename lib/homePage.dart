import 'package:conexcrudfirebase/addStory.dart';
import 'package:conexcrudfirebase/addStory.dart';
import 'package:conexcrudfirebase/one/database.dart';
import 'package:conexcrudfirebase/mostrar.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Database db = new Database();
  List docs = [];
  initialise() {
    db.initiliase();
    db.read().then((value) => {
          setState(() {
            docs = value;
          })
        });
  }

  void eliminar(d) {
    print("liminadndo");
    db.delete(d);
    initialise();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    initialise();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Notas App",
          style: TextStyle(color: Color(0xFFffcb2d)),
        ),
        centerTitle: true,
      ),
      body: Container(
        color: Color(0xFF2b3849),
        child: ListView.builder(
          itemCount: docs.length,
          itemBuilder: (BuildContext contenxt, int index) {
            return Card(
              color: Color(0xFF404a5c),
              margin: EdgeInsets.all(10),
              child: ListTile(
                onTap: () {
                  Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  View(country: docs[index], db: db)))
                      .then((value) => {
                            if (value != null) {initialise()}
                          });
                },
                contentPadding: EdgeInsets.only(right: 30, left: 36),
                title: Text(
                  docs[index]['name'],
                  style: TextStyle(color: Color(0xFFcdd0d5)),
                ),
                trailing: IconButton(
                  icon: const Icon(
                    Icons.library_add_check_outlined,
                    color: Color(0xFF5ac18e), //ff5252
                  ),
                  tooltip: 'Eliminar item',
                  onPressed: () {
                    setState(
                      () {
                        eliminar(docs[index]['id']);
                      },
                    );
                  },
                ),
              ),
            );
          },
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        color: Color(0xFF2b3849),
        child: Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              IconButton(
                icon: Icon(Icons.note_add),
                iconSize: 40,
                color: Color(0xFFffcb2d),
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => AddStory())).then(
                    (value) => {
                      if (value != null) {initialise()}
                    },
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
